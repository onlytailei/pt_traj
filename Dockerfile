FROM osrf/ros:kinetic-desktop-full-xenial

RUN apt-get update

RUN apt-get install -y --no-install-recommends \
    libarmadillo-dev \
    ros-kinetic-control-toolbox \
    ros-kinetic-gazebo-ros-control \
    ros-kinetic-geographic-info \
    ros-kinetic-robot-localization \
    ros-kinetic-ros-control \
    ros-kinetic-ros-controllers \
    ros-kinetic-twist-mux \
    ros-kinetic-interactive-marker-twist-server \
    gdb \
    nano && \
    rm -rf /var/lib/apt/lists/*

RUN mkdir -p /home/catkin_ws/src

WORKDIR /home/catkin_ws/src

ENTRYPOINT ["/bin/bash"]

